Playing Flappy Bird using NEAT algorithm 
===============

![It work really well!](flappy.gif)

I am using a Flappy Bird Clone written by [sourabhv](https://github.com/sourabhv/FlapPyBird)


Try it yourself
================

To run this, you will need:

* python3 with pip installed

pip packages:

* neat-python
* pygame

run by:
`python flappy.py`

Detail
============================

Fitness function: distance traveled

Input:
* Y-position of the bird
* Y-position of the first upper pipe
* Y-position of the first lower pipe
* Y-position of the next upper pipe
* Y-position of the next upper pipe
* Y-velocity of the bird

Output:
* How the bird feel like to jump

Actions:
```
if output >= 0.5 then jump

else do nothing
```
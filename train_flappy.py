# TODO: asynchronous tranning, multiple player at a time
import neat
from itertools import cycle
import random
import sys
import time
import pickle

import pygame
from pygame.locals import *

FPS = 120
SCREENWIDTH  = 288
SCREENHEIGHT = 512
PIPEGAPSIZE  = 100 # gap between upper and lower part of pipe
BASEY        = SCREENHEIGHT * 0.79
# image, sound and hitmask  dicts
IMAGES, HITMASKS = {}, {}

PLAYER = (
        'assets/sprites/bluebird-upflap.png',
        'assets/sprites/bluebird-midflap.png',
        'assets/sprites/bluebird-downflap.png',
)

# list of backgrounds
BG = 'assets/sprites/background-day.png'

# list of pipes
PIPE = 'assets/sprites/pipe-green.png'

try:
    xrange
except NameError:
    xrange = range

movementInfo = {}

def main():
    global SCREEN, FPSCLOCK, SPEED_TOGGLE
    pygame.init()
    SPEED_TOGGLE = False
    FPSCLOCK = pygame.time.Clock()
    SCREEN = pygame.display.set_mode((SCREENWIDTH, SCREENHEIGHT))
    pygame.display.set_caption('Flappy Bird')

    # numbers sprites for score display
    IMAGES['numbers'] = (
        pygame.image.load('assets/sprites/0.png').convert_alpha(),
        pygame.image.load('assets/sprites/1.png').convert_alpha(),
        pygame.image.load('assets/sprites/2.png').convert_alpha(),
        pygame.image.load('assets/sprites/3.png').convert_alpha(),
        pygame.image.load('assets/sprites/4.png').convert_alpha(),
        pygame.image.load('assets/sprites/5.png').convert_alpha(),
        pygame.image.load('assets/sprites/6.png').convert_alpha(),
        pygame.image.load('assets/sprites/7.png').convert_alpha(),
        pygame.image.load('assets/sprites/8.png').convert_alpha(),
        pygame.image.load('assets/sprites/9.png').convert_alpha()
    )

    # base (ground) sprite
    IMAGES['base'] = pygame.image.load('assets/sprites/base.png').convert_alpha()

    IMAGES['background'] = pygame.image.load(BG).convert()

    IMAGES['player'] = (
        pygame.image.load(PLAYER[0]).convert_alpha(),
        pygame.image.load(PLAYER[1]).convert_alpha(),
        pygame.image.load(PLAYER[2]).convert_alpha(),
    )

    IMAGES['pipe'] = (
        pygame.transform.flip(
            pygame.image.load(PIPE).convert_alpha(), False, True),
        pygame.image.load(PIPE).convert_alpha(),
    )

    # hismask for pipes
    HITMASKS['pipe'] = (
        getHitmask(IMAGES['pipe'][0]),
        getHitmask(IMAGES['pipe'][1]),
    )

    # hitmask for player
    HITMASKS['player'] = (
        getHitmask(IMAGES['player'][0]),
        getHitmask(IMAGES['player'][1]),
        getHitmask(IMAGES['player'][2]),
    )

    #train main
    config = neat.Config(neat.DefaultGenome,
     neat.DefaultReproduction,
      neat.DefaultSpeciesSet,
       neat.DefaultStagnation,
        'neat_config')

    popu = neat.Population(config)
    popu.add_reporter(neat.StdOutReporter(True))
    best_bird = popu.run(eval_birds, 300)

    print('Best genome: {}'.format(best_bird))

    #save
    with open('best.bird'.format(best_bird.fitness), 'wb') as f:
        pickle.dump(best_bird, f)

    print('finish.')
    pygame.quit()
    sys.exit()

def eval_birds(genomes, config):
    loopIter = 0
    playerIndexGen = cycle([0, 1, 2, 1])
    playerx = int(SCREENWIDTH * 0.2)

    basex = 0
    baseShift = IMAGES['base'].get_width() - IMAGES['background'].get_width()

    player_height = IMAGES['player'][0].get_height()
    # get 2 new pipes to add to upperPipes lowerPipes list
    newPipe1 = getRandomPipe()
    newPipe2 = getRandomPipe()

    # list of upper pipes
    upperPipes = [
        {'x': SCREENWIDTH + 200, 'y': newPipe1[0]['y']},
        {'x': SCREENWIDTH + 200 + (SCREENWIDTH / 2), 'y': newPipe2[0]['y']},
    ]

    # list of lowerpipe
    lowerPipes = [
        {'x': SCREENWIDTH + 200, 'y': newPipe1[1]['y']},
        {'x': SCREENWIDTH + 200 + (SCREENWIDTH / 2), 'y': newPipe2[1]['y']},
    ]

    pipeVelX = -4

    # player velocity, max velocity, downward accleration, accleration on flap
    playerMaxVelY =  10   # max vel along Y, max descend speed
    playerMinVelY =  -8   # min vel along Y, max ascend speed
    playerAccY    =   1   # players downward accleration
    playerVelRot  =   3   # angular speed
    playerRotThr  =  20   # rotation threshold
    playerFlapAcc =  -9   # players speed on flapping
    #all bird shared same distance counter
    playerTraveled = 0

    birds = []
    # create bird brains
    for bird_id, g in genomes:
        bird = {}
        bird['id'] = bird_id
        bird['brain'] = neat.nn.FeedForwardNetwork.create(g, config)
        bird['velY'] = -9 # player's velocity along Y, default same as playerFlapped
        bird['flapped'] = False
        bird['y'] = int((SCREENHEIGHT - IMAGES['player'][0].get_height()) / 2)
        bird['rotate'] = 45
        bird['idx'] = 0
        bird['fitness'] = 0
        bird['dead'] = False
        birds.append(bird)

    bird_alive = len(genomes)
    score = 0
    SPEED_TOGGLE = True
    while True:

        if bird_alive == 0:
            for i in range(len(genomes)):
                genomes[i][1].fitness = birds[i]['fitness']
            return

        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                return

            if event.type == KEYDOWN and event.key == K_SPACE:
                SPEED_TOGGLE = not SPEED_TOGGLE
                print('speed toggle:', SPEED_TOGGLE)
        #get actions from the birds
        for bird in birds:
            if bird['dead'] == True:
                continue
            norm_y = 1 - bird['y'] / SCREENHEIGHT
            norm_upper_x = upperPipes[0]['x'] - playerx
            norm_upper_y = upperPipes[0]['y'] - bird['y']
            norm_lower_y = lowerPipes[0]['y'] - bird['y']
            norm_next_upper_y = upperPipes[1]['y'] - bird['y']
            norm_next_lower_y = lowerPipes[1]['y'] - bird['y']

            action = bird['brain'].activate([norm_y, norm_upper_x, bird['velY'],
                                                 norm_upper_y, norm_lower_y, norm_next_upper_y, norm_next_lower_y])
            if action[0] < 0.5:
                if bird['y'] > -2 * IMAGES['player'][0].get_height():
                    bird['velY'] = playerFlapAcc
                    bird['flapped'] = True

        # check for crash here
        for bird in birds:
            if bird['dead'] == True:
                continue
            crashTest = checkCrash({'x': playerx, 'y': bird['y'], 'index': bird['idx']},
                                   upperPipes, lowerPipes)
            #if we crashed or being flying too high or crash to the ground
            if crashTest[0] or bird['y'] < 0 or (bird['y'] + player_height >= BASEY - 1):
                # we dont want a plane or a car
                if bird['y'] <= 0 or (bird['y'] + player_height >= BASEY - 1):
                    fitness = -playerTraveled
                else:
                    fitness = playerTraveled
                bird['fitness'] = fitness
                bird_alive -= 1
                bird['dead'] = True

        playerMidPos = playerx + IMAGES['player'][0].get_width() / 2
        for pipe in upperPipes:
            pipeMidPos = pipe['x'] + IMAGES['pipe'][0].get_width() / 2
            if pipeMidPos <= playerMidPos < pipeMidPos + 4:
                score += 1

        # playerIndex basex change
        if (loopIter + 1) % 3 == 0:
            for bird in birds:
                if bird['dead'] == True:
                    continue
                bird['idx'] = next(playerIndexGen)
        loopIter = (loopIter + 1) % 30
        basex = -((-basex + 100) % baseShift)

        # rotate the birds
        for bird in birds:
            if bird['dead'] == True:
                continue
            if bird['rotate'] > -90:
                bird['rotate'] -= playerVelRot

        # player's movement
        for bird in birds:
            if bird['dead'] == True:
                continue
            if bird['velY'] < playerMaxVelY and not bird['flapped']:
                bird['velY'] += playerAccY
            if bird['flapped']:
                bird['flapped'] = False
                # more rotation to cover the threshold (calculated in visible rotation)
                bird['rotate'] = 45

            playerHeight = IMAGES['player'][bird['idx']].get_height()
            bird['y'] += min(bird['velY'], BASEY - bird['y'] - playerHeight)

        # move pipes to left
        for uPipe, lPipe in zip(upperPipes, lowerPipes):
            uPipe['x'] += pipeVelX
            lPipe['x'] += pipeVelX

        #count distance
        playerTraveled += 1

        # remove first pipe if the bird passed it
        if upperPipes[0]['x'] < playerx - ((IMAGES['pipe'][0].get_width() / 2) + 16):
            upperPipes.pop(0)
            lowerPipes.pop(0)
            newPipe = getRandomPipe()
            upperPipes.append(newPipe[0])
            lowerPipes.append(newPipe[1])

        # draw sprites
        SCREEN.blit(IMAGES['background'], (0,0))

        for uPipe, lPipe in zip(upperPipes, lowerPipes):
            SCREEN.blit(IMAGES['pipe'][0], (uPipe['x'], uPipe['y']))
            SCREEN.blit(IMAGES['pipe'][1], (lPipe['x'], lPipe['y']))

        SCREEN.blit(IMAGES['base'], (basex, BASEY))
        # print score so player overlaps the score
        showScore(score)
        showDistance(playerTraveled)

        # Player rotation has a threshold
        for bird in birds:
            if bird['dead'] == True:
                continue
            visibleRot = playerRotThr
            if bird['rotate'] <= playerRotThr:
                visibleRot = bird['rotate']

            playerSurface = pygame.transform.rotate(IMAGES['player'][bird['idx']], visibleRot)
            SCREEN.blit(playerSurface, (playerx, bird['y']))

        pygame.display.update()
        if not SPEED_TOGGLE:
            FPSCLOCK.tick(FPS)

def playerShm(playerShm):
    """oscillates the value of playerShm['val'] between 8 and -8"""
    if abs(playerShm['val']) == 8:
        playerShm['dir'] *= -1

    if playerShm['dir'] == 1:
         playerShm['val'] += 1
    else:
        playerShm['val'] -= 1


def getRandomPipe():
    """returns a randomly generated pipe"""
    # y of gap between upper and lower pipe
    gapY = random.randrange(0, int(BASEY * 0.6 - PIPEGAPSIZE))
    gapY += int(BASEY * 0.2)
    pipeHeight = IMAGES['pipe'][0].get_height()
    pipeX = SCREENWIDTH + 10

    return [
        {'x': pipeX, 'y': gapY - pipeHeight},  # upper pipe
        {'x': pipeX, 'y': gapY + PIPEGAPSIZE}, # lower pipe
    ]

def showDistance(distance):
    distanceDigits = [int(x) for x in list(str(distance))]
    totalWidth = 0 # total width of all numbers to be printed

    for digit in distanceDigits:
        totalWidth += IMAGES['numbers'][digit].get_width()

    Xoffset = (SCREENWIDTH - totalWidth) / 2

    for digit in distanceDigits:
        SCREEN.blit(IMAGES['numbers'][digit], (Xoffset, SCREENHEIGHT * 0.85))
        Xoffset += IMAGES['numbers'][digit].get_width()

def showScore(score):
    """displays score in center of screen"""
    scoreDigits = [int(x) for x in list(str(score))]
    totalWidth = 0 # total width of all numbers to be printed

    for digit in scoreDigits:
        totalWidth += IMAGES['numbers'][digit].get_width()

    Xoffset = (SCREENWIDTH - totalWidth) / 2

    for digit in scoreDigits:
        SCREEN.blit(IMAGES['numbers'][digit], (Xoffset, SCREENHEIGHT * 0.1))
        Xoffset += IMAGES['numbers'][digit].get_width()


def checkCrash(player, upperPipes, lowerPipes):
    """returns True if player collders with base or pipes."""
    pi = player['index']
    player['w'] = IMAGES['player'][0].get_width()
    player['h'] = IMAGES['player'][0].get_height()

    if True:

        playerRect = pygame.Rect(player['x'], player['y'],
                      player['w'], player['h'])
        pipeW = IMAGES['pipe'][0].get_width()
        pipeH = IMAGES['pipe'][0].get_height()

        for uPipe, lPipe in zip(upperPipes, lowerPipes):
            # upper and lower pipe rects
            uPipeRect = pygame.Rect(uPipe['x'], uPipe['y'], pipeW, pipeH)
            lPipeRect = pygame.Rect(lPipe['x'], lPipe['y'], pipeW, pipeH)

            # player and upper/lower pipe hitmasks
            pHitMask = HITMASKS['player'][pi]
            uHitmask = HITMASKS['pipe'][0]
            lHitmask = HITMASKS['pipe'][1]

            # if bird collided with upipe or lpipe
            uCollide = pixelCollision(playerRect, uPipeRect, pHitMask, uHitmask)
            lCollide = pixelCollision(playerRect, lPipeRect, pHitMask, lHitmask)

            if uCollide or lCollide:
                return [True, False]

    return [False, False]

def pixelCollision(rect1, rect2, hitmask1, hitmask2):
    """Checks if two objects collide and not just their rects"""
    rect = rect1.clip(rect2)

    if rect.width == 0 or rect.height == 0:
        return False

    x1, y1 = rect.x - rect1.x, rect.y - rect1.y
    x2, y2 = rect.x - rect2.x, rect.y - rect2.y

    for x in xrange(rect.width):
        for y in xrange(rect.height):
            if hitmask1[x1+x][y1+y] and hitmask2[x2+x][y2+y]:
                return True
    return False

def getHitmask(image):
    """returns a hitmask using an image's alpha."""
    mask = []
    for x in xrange(image.get_width()):
        mask.append([])
        for y in xrange(image.get_height()):
            mask[x].append(bool(image.get_at((x,y))[3]))
    return mask

if __name__ == '__main__':
    main()
